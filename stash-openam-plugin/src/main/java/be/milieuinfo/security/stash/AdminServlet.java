package be.milieuinfo.security.stash;

import java.io.IOException;
import java.net.URI;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

public class AdminServlet extends HttpServlet {
	protected static final String ADMIN_TEMPLATE = "admin.vm";
	private static final long serialVersionUID = 4956547031369518402L;
	private final UserManager userManager;
	private final LoginUriProvider loginUriProvider;
	private final TemplateRenderer renderer;

	public AdminServlet(UserManager userManager, LoginUriProvider loginUriProvider, TemplateRenderer renderer) {
		super();
		this.userManager = userManager;
		this.loginUriProvider = loginUriProvider;
		this.renderer = renderer;
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (!isUserAdmin(request)) {
			redirectToLogin(request, response);
			return;
		}
		response.setContentType("text/html;charset=utf-8");
		renderer.render(ADMIN_TEMPLATE, response.getWriter());
	}

	private boolean isUserAdmin(HttpServletRequest request) {
		UserKey userKey = userManager.getRemoteUserKey(request);
		if (userKey != null && userManager.isSystemAdmin(userKey)) {
			return true;
		}
		return false;
	}


	private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
	}

	private URI getUri(HttpServletRequest request) {
		StringBuffer builder = request.getRequestURL();
		if (request.getQueryString() != null) {
			builder.append("?");
			builder.append(request.getQueryString());
		}
		return URI.create(builder.toString());
	}
}
