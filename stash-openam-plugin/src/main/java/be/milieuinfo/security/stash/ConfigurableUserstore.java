package be.milieuinfo.security.stash;

import be.milieuinfo.security.openam.oauth.OAuthTokenPair;

public interface ConfigurableUserstore {
	void setServerUrl(String serverUrl);

	public void setConsumerToken(OAuthTokenPair consumerToken);

}
