package be.milieuinfo.security.stash;

import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;


public class IsUserAdmin implements Condition {
	private final UserManager userManager;

	public IsUserAdmin(UserManager userManager) {
		super();
		this.userManager = userManager;
	}

	@Override
	public void init(Map<String, String> params) throws PluginParseException {
	}

	@Override
	public boolean shouldDisplay(Map<String, Object> params) {
		UserKey remoteUserKey = userManager.getRemoteUserKey();
		if (remoteUserKey != null && userManager.isSystemAdmin(remoteUserKey)) {
			return true;
		}
		return false;
	}
}
