package be.milieuinfo.security.stash;

import be.milieuinfo.security.openam.api.OpenAMIdentityService;
import be.milieuinfo.security.openam.api.OpenAMUserdetails;

import com.atlassian.cache.CacheLoader;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.util.concurrent.NotNull;

public class OpenAMAuthCacheLoader implements CacheLoader<String, String> {
	private final OpenAMIdentityService identityService;
	private final I18nService i18nService;

	public OpenAMAuthCacheLoader(OpenAMIdentityService identityService, I18nService i18nService) {
		super();
		this.identityService = identityService;
		this.i18nService = i18nService;
	}

	@Override
	public String load(@NotNull String ssoTokenId) {
		OpenAMUserdetails userdetails = this.identityService.getUserDetails(ssoTokenId);
		if (userdetails != null) {
			return userdetails.getUsername();
		}
		throw new OpenAMAuthException(this.i18nService.createKeyedMessage(
				OpenAMAuthHandler.OPENAM_AUTH_MISSING_MSG_KEY, new Object[0]));
	}

}
