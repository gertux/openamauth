package be.milieuinfo.security.stash;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import be.milieuinfo.security.openam.oauth.OAuthTokenPair;

import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

public class OpenAMAuthConfig implements StoredConfig {
	private static final String PREFIX = "be.milieuinfo.security.stash.openamauth.";
	private static final Logger LOGGER = LoggerFactory.getLogger(OpenAMAuthConfig.class);
	private final PluginSettingsFactory pluginSettingsFactory;
	private final ConfigurableUserstore userStore;
	private Boolean enabled;
	private String publicUrl;

	public OpenAMAuthConfig(PluginSettingsFactory pluginSettingsFactory, ConfigurableUserstore userStore) {
		super();
		this.pluginSettingsFactory = pluginSettingsFactory;
		this.userStore = userStore;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.milieuinfo.security.stash.StoredConfig#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		if (this.enabled == null) {
			this.enabled = Boolean.parseBoolean(getSetting(Key.ENABLED));
			this.userStore.setServerUrl(getServerUrl());
			this.userStore.setConsumerToken(getConsumerToken());
		}
		return this.enabled;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.milieuinfo.security.stash.StoredConfig#setEnabled(boolean)
	 */
	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		putSetting(Key.ENABLED, this.enabled.toString());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.milieuinfo.security.stash.StoredConfig#getServerUrl()
	 */
	@Override
	public String getServerUrl() {
		return getSetting(Key.SERVERURL);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.milieuinfo.security.stash.StoredConfig#setServerUrl(java.lang.String)
	 */
	@Override
	public void setServerUrl(String serverUrl) {
		putSetting(Key.SERVERURL, serverUrl);
		this.userStore.setServerUrl(serverUrl);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.milieuinfo.security.stash.StoredConfig#getPublicUrl()
	 */
	@Override
	public String getPublicUrl() {
		if (this.publicUrl == null) {
			this.publicUrl = getSetting(Key.PUBLICURL);
		}
		return this.publicUrl;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.milieuinfo.security.stash.StoredConfig#setPublicUrl(java.lang.String)
	 */
	@Override
	public void setPublicUrl(String publicUrl) {
		putSetting(Key.PUBLICURL, publicUrl);
		this.publicUrl = publicUrl;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.milieuinfo.security.stash.StoredConfig#getConsumerToken()
	 */
	@Override
	public OAuthTokenPair getConsumerToken() {
		return new OAuthTokenPair(getSetting(Key.CONSUMERNAME), getSetting(Key.CONSUMERSECRET));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * be.milieuinfo.security.stash.StoredConfig#setConsumerToken(be.milieuinfo.security.openam.oauth.OAuthTokenPair)
	 */
	@Override
	public void setConsumerToken(OAuthTokenPair consumerToken) {
		putSetting(Key.CONSUMERNAME, consumerToken.getToken());
		putSetting(Key.CONSUMERSECRET, consumerToken.getSecret());
		this.userStore.setConsumerToken(consumerToken);
	}

	private boolean putSetting(final Key key, final String newValue) {
		try {
			this.pluginSettingsFactory.createGlobalSettings().put(key.getKey(), newValue);
			return true;
		} catch (RuntimeException e) {
			LOGGER.warn("Couldn't change the setting. This can safely be ignored during plugin shutdown. Detail: "
					+ e.getMessage());
			return false;
		}
	}

	private String getSetting(final Key key) {
		try {
			return (String) this.pluginSettingsFactory.createGlobalSettings().get(key.getKey());
		} catch (RuntimeException e) {
			LOGGER.warn("Couldn't check the setting. This can safely be ignored during plugin shutdown. Detail: "
					+ e.getMessage());
			return null;
		}
	}

	public enum Key {
		ENABLED("enabled"), SERVERURL("serverurl"), PUBLICURL("publicurl"), CONSUMERNAME("consumername"), CONSUMERSECRET(
				"consumersecret");

		private String key;

		private Key(final String suffix) {
			this.key = PREFIX + suffix;
		}

		public String getKey() {
			return this.key;
		}

	}
}
