package be.milieuinfo.security.stash;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import be.milieuinfo.security.openam.oauth.OAuthTokenPair;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;

@Path("/")
public class OpenAMAuthConfigResource {
	private final UserManager userManager;
	private final StoredConfig storedConfig;

	public OpenAMAuthConfigResource(UserManager userManager, StoredConfig storedConfig) {
		super();
		this.userManager = userManager;
		this.storedConfig = storedConfig;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context HttpServletRequest request) {
		if (!isUserAdmin(request)) {
			return Response.status(Status.UNAUTHORIZED).build();
		}
		Config config = new Config(storedConfig.isEnabled(), storedConfig.getServerUrl(), storedConfig.getPublicUrl(), storedConfig.getConsumerToken()
				.getToken(), storedConfig.getConsumerToken().getSecret());
		return Response.ok(config).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response put(final Config config, @Context HttpServletRequest request) {
		if (!isUserAdmin(request)) {
			return Response.status(Status.UNAUTHORIZED).build();
		}
		storedConfig.setEnabled(config.isEnabled());
		storedConfig.setServerUrl(config.getServerUrl());
		storedConfig.setPublicUrl(config.getPublicUrl());
		storedConfig.setConsumerToken(new OAuthTokenPair(config.getConsumerName(), config.getConsumerSecret()));
		return Response.noContent().build();
	}

	private boolean isUserAdmin(HttpServletRequest request) {
		UserKey userKey = userManager.getRemoteUserKey(request);
		if (userKey != null && userManager.isSystemAdmin(userKey)) {
			return true;
		}
		return false;
	}

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	public static final class Config {
		@XmlElement
		private boolean enabled;
		@XmlElement
		private String serverUrl;
		@XmlElement
		private String publicUrl;
		@XmlElement
		private String consumerName;
		@XmlElement
		private String consumerSecret;

		public Config() {
			super();
		}

		public Config(boolean enabled, String serverUrl, String publicUrl, String consumerName, String consumerSecret) {
			super();
			this.enabled = enabled;
			this.serverUrl = serverUrl;
			this.publicUrl = publicUrl;
			this.consumerName = consumerName;
			this.consumerSecret = consumerSecret;
		}

		public boolean isEnabled() {
			return enabled;
		}

		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}

		public String getServerUrl() {
			return serverUrl;
		}

		public void setServerUrl(String serverUrl) {
			this.serverUrl = serverUrl;
		}

		public String getPublicUrl() {
			return publicUrl;
		}

		public void setPublicUrl(String publicUrl) {
			this.publicUrl = publicUrl;
		}

		public String getConsumerName() {
			return consumerName;
		}

		public void setConsumerName(String consumerName) {
			this.consumerName = consumerName;
		}

		public String getConsumerSecret() {
			return consumerSecret;
		}

		public void setConsumerSecret(String consumerSecret) {
			this.consumerSecret = consumerSecret;
		}
	}
}
