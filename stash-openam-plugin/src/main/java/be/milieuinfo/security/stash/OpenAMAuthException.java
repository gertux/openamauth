package be.milieuinfo.security.stash;

import com.atlassian.stash.i18n.KeyedMessage;
import com.atlassian.stash.user.AuthenticationException;

public class OpenAMAuthException extends AuthenticationException {
	private static final long serialVersionUID = -6887551657342695553L;

	public OpenAMAuthException(KeyedMessage message) {
		super(message);
	}
}
