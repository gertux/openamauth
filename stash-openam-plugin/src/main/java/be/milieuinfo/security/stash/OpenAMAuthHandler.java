package be.milieuinfo.security.stash;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import be.milieuinfo.security.openam.api.OpenAMIdentityService;
import be.milieuinfo.security.openam.common.OpenAMTools;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettings;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.stash.auth.HttpAuthenticationContext;
import com.atlassian.stash.auth.HttpAuthenticationFailureContext;
import com.atlassian.stash.auth.HttpAuthenticationFailureHandler;
import com.atlassian.stash.auth.HttpAuthenticationHandler;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.user.ExpiredAuthenticationException;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.user.UserService;

public class OpenAMAuthHandler implements HttpAuthenticationHandler, HttpAuthenticationFailureHandler {
	protected static final String OPENAM_AUTH_NS = "openam.auth";
	protected static final String OPENAM_AUTH_EXPIRED_MSG_KEY = OPENAM_AUTH_NS + ".expired";
	protected static final String OPENAM_AUTH_MISSING_MSG_KEY = OPENAM_AUTH_NS + ".missing";
	protected static final String OPENAM_AUTH_CACHE = OPENAM_AUTH_NS + ".cache";
	protected static final String LOGIN_PATHINFO = "/login";
	private static final Logger LOGGER = LoggerFactory.getLogger(OpenAMAuthHandler.class);
	private final UserService userService;
	private final StoredConfig storedConfig;
	private final I18nService i18nService;
	private Cache<String, String> authenticationCache;

	public OpenAMAuthHandler(final UserService userService, final OpenAMIdentityService identityService,
			final CacheManager cacheManager, final StoredConfig storedConfig, final I18nService i18nService) {
		this.userService = userService;
		this.storedConfig = storedConfig;
		this.i18nService = i18nService;
		CacheSettings cacheSettings = new CacheSettingsBuilder().expireAfterWrite(15, TimeUnit.MINUTES).build();
		this.authenticationCache = cacheManager.getCache(OPENAM_AUTH_CACHE, new OpenAMAuthCacheLoader(identityService,
				i18nService), cacheSettings);
	}

	@Override
	public StashUser authenticate(HttpAuthenticationContext context) {
		if (requiresAuthentication(context)) {
			String token = getTokenFromRequest(context.getRequest());
			if (token != null) {
				String username = this.authenticationCache.get(token);
				if (username != null) {
					return this.userService.getUserByName(username);
				}
			}
			throw new OpenAMAuthException(this.i18nService.createKeyedMessage(OPENAM_AUTH_MISSING_MSG_KEY,
					new Object[0]));
		}
		return null;
	}

	@Override
	public void validateAuthentication(HttpAuthenticationContext context) {
		if (requiresAuthentication(context)) {
			String token = getTokenFromRequest(context.getRequest());
			if (token != null) {
				try {
					String username = this.authenticationCache.get(token);
					if (username != null) {
						return;
					}
				} catch (OpenAMAuthException e) {
					LOGGER.debug(OPENAM_AUTH_EXPIRED_MSG_KEY, e);
				}
				throw new ExpiredAuthenticationException(this.i18nService.createKeyedMessage(
						OPENAM_AUTH_EXPIRED_MSG_KEY, new Object[0]));
			}
		}
	}

	private boolean requiresAuthentication(HttpAuthenticationContext context) {
		return (this.storedConfig.isEnabled() && HttpAuthenticationContext.METHOD_TOKEN.equals(context.getMethod()) && !LOGIN_PATHINFO
				.equals(context.getRequest().getPathInfo()));
	}

	@Override
	public boolean onAuthenticationFailure(HttpAuthenticationFailureContext context) throws ServletException,
			IOException {
		if (requiresAuthentication(context) && (context.getAuthenticationException() instanceof OpenAMAuthException)) {
			context.getResponse().sendRedirect(getLoginUrl(context.getRequest()));
			return true;
		}
		return false;
	}

	protected String getTokenFromRequest(HttpServletRequest request) {
		String token = OpenAMTools.getSSOTokenID(request);
		if (token == null) {
			token = OpenAMTools.getSSOTokenFromCookies(request.getCookies());
		}
		return token;
	}

	protected String getLoginUrl(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder(this.storedConfig.getPublicUrl());
		try {
			sb.append("?goto=").append(URLEncoder.encode(request.getRequestURL().toString(), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("Unable to encode goto URL", e);
		}
		return sb.toString();
	}
}
