package be.milieuinfo.security.stash;

import be.milieuinfo.security.openam.oauth.OAuthTokenPair;

public interface StoredConfig {

	public abstract boolean isEnabled();

	public abstract void setEnabled(boolean enabled);

	public abstract String getServerUrl();

	public abstract void setServerUrl(String serverUrl);

	public abstract String getPublicUrl();

	public abstract void setPublicUrl(String publicUrl);

	public abstract OAuthTokenPair getConsumerToken();

	public abstract void setConsumerToken(OAuthTokenPair consumerToken);

}