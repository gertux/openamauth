AJS.toInit(function() {
  var baseUrl = AJS.$("meta[name='application-base-url']").attr("content");
    
  function populateForm() {
    AJS.$.ajax({
      url: baseUrl + "/rest/openamsso-admin/1.0/",
      dataType: "json",
      success: function(config) {
      	if(config.enabled) {
      		AJS.$("#enabled").attr("checked", true);
    	} else {
      		AJS.$("#disabled").attr("checked", true);
    	}
        AJS.$("#serverUrl").attr("value", config.serverUrl);
        AJS.$("#publicUrl").attr("value", config.publicUrl);
        AJS.$("#consumerName").attr("value", config.consumerName);
        AJS.$("#consumerSecret").attr("value", config.consumerSecret);
      }
    });
  }
  function updateConfig() {
    AJS.$.ajax({
      url: baseUrl + "/rest/openamsso-admin/1.0/",
      type: "PUT",
      contentType: "application/json",
      data: '{ "enabled": ' + AJS.$("#enabled").attr("value") + ', "serverUrl": "' +  AJS.$("#serverUrl").attr("value") + '", "publicUrl": "' +  AJS.$("#publicUrl").attr("value") + '", "consumerName": "' +  AJS.$("#consumerName").attr("value") + '", "consumerSecret": "' +  AJS.$("#consumerSecret").attr("value") + '" }',
      processData: false
    });
  }  
  populateForm();

  AJS.$("#admin").submit(function(e) {
    e.preventDefault();
    updateConfig();
  });
});
