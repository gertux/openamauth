package be.milieuinfo.security.stash;

import static be.milieuinfo.security.stash.AdminServlet.ADMIN_TEMPLATE;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

public class AdminServletTest {
	private static final String REQUEST_URL = "http://server/stash";
	private static final URI LOGINURI = URI.create(REQUEST_URL + "/login");
	@Mock
	private UserManager userManager;
	private UserKey userKey;
	@Mock
	private LoginUriProvider loginUriProvider;
	@Mock
	private TemplateRenderer renderer;
	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;
	@Mock
	private PrintWriter writer;

	private AdminServlet servlet;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		StringBuffer requestURL = new StringBuffer(REQUEST_URL);
		when(this.request.getRequestURL()).thenReturn(requestURL);
		when(this.response.getWriter()).thenReturn(writer);
		when(this.loginUriProvider.getLoginUri(any(URI.class))).thenReturn(LOGINURI);
		this.userKey = new UserKey("");
		servlet = new AdminServlet(userManager, loginUriProvider, renderer);
	}

	@Test
	public void testDoGet() throws IOException {
		when(this.userManager.getRemoteUserKey(eq(request))).thenReturn(userKey);
		when(this.userManager.isSystemAdmin(eq(userKey))).thenReturn(Boolean.TRUE);

		servlet.doGet(request, response);

		verify(renderer).render(eq(ADMIN_TEMPLATE), eq(writer));
	}

	@Test
	public void testDoGetNoUser() throws IOException {
		when(this.userManager.getRemoteUserKey(eq(request))).thenReturn(null);
		when(this.userManager.isSystemAdmin(eq(userKey))).thenReturn(Boolean.TRUE);

		servlet.doGet(request, response);

		verify(renderer, never()).render(eq(ADMIN_TEMPLATE), eq(writer));
	}

	@Test
	public void testDoGetNoAdmin() throws IOException {
		when(this.userManager.getRemoteUserKey(eq(request))).thenReturn(userKey);
		when(this.userManager.isSystemAdmin(eq(userKey))).thenReturn(Boolean.FALSE);

		servlet.doGet(request, response);

		verify(renderer, never()).render(eq(ADMIN_TEMPLATE), eq(writer));
	}

}
