package be.milieuinfo.security.stash;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;

public class IsUserAdminTest {
	@Mock
	private UserManager userManager;
	private UserKey userKey;
	private Map<String, Object> params;
	private IsUserAdmin condition;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.userKey = new UserKey("");
		this.condition = new IsUserAdmin(this.userManager);
	}

	@Test
	public void testShouldDisplay() {
		when(this.userManager.getRemoteUserKey()).thenReturn(userKey);
		when(this.userManager.isSystemAdmin(eq(userKey))).thenReturn(Boolean.TRUE);

		assertTrue(condition.shouldDisplay(params));
	}

	@Test
	public void testShouldDisplayNoUser() {
		when(this.userManager.getRemoteUserKey()).thenReturn(null);
		when(this.userManager.isSystemAdmin(eq(userKey))).thenReturn(Boolean.TRUE);

		assertFalse(condition.shouldDisplay(params));
	}

	@Test
	public void testShouldDisplayNoAdmin() {
		when(this.userManager.getRemoteUserKey()).thenReturn(userKey);
		when(this.userManager.isSystemAdmin(eq(userKey))).thenReturn(Boolean.FALSE);

		assertFalse(condition.shouldDisplay(params));
	}

}
