package be.milieuinfo.security.stash;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import be.milieuinfo.security.openam.api.OpenAMIdentityService;
import be.milieuinfo.security.openam.api.OpenAMUserdetails;

import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.i18n.KeyedMessage;

public class OpenAMAuthCacheLoaderTest {
	private static final String SSO_TOKENID = "AQIC5wM2LY4RfckcedfzxGrgVYevbKR-SgBkuemF4Cmn5Qg.*AAJTSQABMDE.*";
	private static final String SSO_TOKENID_NODETAILS = "YevbKRAQIC5wM2LY4RfckcedfzxGrgV-SgBkuemF4Cmn5Qg.*AAJTSQABMDE.*";
	private static final String USERNAME = "jrr";
	private static final KeyedMessage MSG = new KeyedMessage(OpenAMAuthHandler.OPENAM_AUTH_EXPIRED_MSG_KEY, "msg",
			"rootMessage");
	@Mock
	private OpenAMIdentityService identityService;
	@Mock
	private OpenAMUserdetails userdetails;
	@Mock
	private I18nService i18nService;
	private OpenAMAuthCacheLoader loader;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.loader = new OpenAMAuthCacheLoader(this.identityService, this.i18nService);
		when(this.identityService.getUserDetails(eq(SSO_TOKENID))).thenReturn(this.userdetails);
		when(this.userdetails.getUsername()).thenReturn(USERNAME);
		when(
				this.i18nService.createKeyedMessage(eq(OpenAMAuthHandler.OPENAM_AUTH_MISSING_MSG_KEY),
						Matchers.anyVararg())).thenReturn(MSG);
	}

	@Test
	public void testLoad() {
		assertEquals(USERNAME, this.loader.load(SSO_TOKENID));
	}

	@Test(expected = OpenAMAuthException.class)
	public void testLoadNoDetails() {
		this.loader.load(SSO_TOKENID_NODETAILS);
	}
}
