package be.milieuinfo.security.stash;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import be.milieuinfo.security.openam.oauth.OAuthTokenPair;
import be.milieuinfo.security.stash.OpenAMAuthConfigResource.Config;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;

public class OpenAMAuthConfigResourceTest {
	private static final boolean ENABLED = true;
	private static final String SERVERURL = "http://server/sso";
	private static final String PUBLICURL = "http://www/sso";
	private static final String CONSUMERNAME = "johndoe";
	private static final String CONSUMERSECRET = "qpwoeiruty";
	private static final OAuthTokenPair CONSUMERTOKENPAIR = new OAuthTokenPair(CONSUMERNAME, CONSUMERSECRET);
	private static final Config CONFIG = new Config(ENABLED, SERVERURL, PUBLICURL, CONSUMERNAME, CONSUMERSECRET);
	@Mock
	private UserManager userManager;
	private UserKey userKey;
	@Mock
	private StoredConfig storedConfig;
	@Mock
	private HttpServletRequest request;
	private OpenAMAuthConfigResource configResource;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.userKey = new UserKey("");
		when(this.storedConfig.getConsumerToken()).thenReturn(CONSUMERTOKENPAIR);
		when(this.storedConfig.getServerUrl()).thenReturn(SERVERURL);
		when(this.storedConfig.getPublicUrl()).thenReturn(PUBLICURL);
		when(this.storedConfig.isEnabled()).thenReturn(ENABLED);
		this.configResource = new OpenAMAuthConfigResource(userManager, storedConfig);
	}

	@Test
	public void testGet() {
		when(this.userManager.getRemoteUserKey(eq(request))).thenReturn(userKey);
		when(this.userManager.isSystemAdmin(eq(userKey))).thenReturn(Boolean.TRUE);

		Response response = configResource.get(request);
		assertEquals(Status.OK.getStatusCode(), response.getStatus());
		assertTrue(response.getEntity() instanceof Config);
		Config config = (Config) response.getEntity();
		assertEquals(ENABLED, config.isEnabled());
		assertEquals(SERVERURL, config.getServerUrl());
		assertEquals(PUBLICURL, config.getPublicUrl());
		assertEquals(CONSUMERNAME, config.getConsumerName());
		assertEquals(CONSUMERSECRET, config.getConsumerSecret());
	}

	@Test
	public void testGetNoUser() {
		when(this.userManager.getRemoteUserKey(eq(request))).thenReturn(null);
		when(this.userManager.isSystemAdmin(eq(userKey))).thenReturn(Boolean.TRUE);

		Response response = configResource.get(request);
		assertEquals(Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
	}

	@Test
	public void testGetNoAdmin() {
		when(this.userManager.getRemoteUserKey(eq(request))).thenReturn(userKey);
		when(this.userManager.isSystemAdmin(eq(userKey))).thenReturn(Boolean.FALSE);

		Response response = configResource.get(request);
		assertEquals(Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
	}

	@Test
	public void testPut() {
		when(this.userManager.getRemoteUserKey(eq(request))).thenReturn(userKey);
		when(this.userManager.isSystemAdmin(eq(userKey))).thenReturn(Boolean.TRUE);
		TokenCatcher tokenCatcher = new TokenCatcher();
		doNothing().when(this.storedConfig).setConsumerToken(argThat(tokenCatcher));

		Response response = configResource.put(CONFIG, request);
		assertEquals(Status.OK.getStatusCode(), response.getStatus(), response.getStatus());
		assertEquals(CONSUMERNAME, tokenCatcher.getTokenPair().getToken());
		assertEquals(CONSUMERSECRET, tokenCatcher.getTokenPair().getSecret());
		verify(this.storedConfig).setEnabled(eq(ENABLED));
		verify(this.storedConfig).setServerUrl(eq(SERVERURL));
		verify(this.storedConfig).setPublicUrl(eq(PUBLICURL));
	}

	@Test
	public void testPutNoUser() {
		when(this.userManager.getRemoteUserKey(eq(request))).thenReturn(null);
		when(this.userManager.isSystemAdmin(eq(userKey))).thenReturn(Boolean.TRUE);

		Response response = configResource.put(CONFIG, request);
		assertEquals(Status.UNAUTHORIZED.getStatusCode(), response.getStatus(), response.getStatus());
	}

	@Test
	public void testPutNoAdmin() {
		when(this.userManager.getRemoteUserKey(eq(request))).thenReturn(userKey);
		when(this.userManager.isSystemAdmin(eq(userKey))).thenReturn(Boolean.FALSE);

		Response response = configResource.put(CONFIG, request);
		assertEquals(Status.UNAUTHORIZED.getStatusCode(), response.getStatus(), response.getStatus());
	}

	public static class TokenCatcher extends ArgumentMatcher<OAuthTokenPair> {
		private OAuthTokenPair tokenPair;

		public final OAuthTokenPair getTokenPair() {
			return this.tokenPair;
		}

		@Override
		public boolean matches(Object argument) {
			if (argument instanceof OAuthTokenPair) {
				this.tokenPair = (OAuthTokenPair) argument;
				return true;
			}
			return false;
		}
	}
}
