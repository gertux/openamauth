package be.milieuinfo.security.stash;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import be.milieuinfo.security.openam.oauth.OAuthTokenPair;
import be.milieuinfo.security.stash.OpenAMAuthConfig.Key;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

public class OpenAMAuthConfigTest {
	private static final String SERVERURL = "http://server/sso";
	private static final String PUBLICURL = "http://www/sso";
	private static final String CONSUMERNAME = "johndoe";
	private static final String CONSUMERSECRET = "qpwoeiruty";
	private static final OAuthTokenPair CONSUMERTOKENPAIR = new OAuthTokenPair(CONSUMERNAME, CONSUMERSECRET);
	private StoredConfig config;
	@Mock
	private PluginSettingsFactory pluginSettingsFactory;
	@Mock
	private ConfigurableUserstore userStore;
	@Mock
	private PluginSettings pluginSettings;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		when(this.pluginSettingsFactory.createGlobalSettings()).thenReturn(this.pluginSettings);
		this.config = new OpenAMAuthConfig(this.pluginSettingsFactory, this.userStore);
	}

	@Test
	public void testIsEnabledTrue() {
		OAuthTokenPairCatcher catcher = new OAuthTokenPairCatcher();
		doNothing().when(this.userStore).setConsumerToken(argThat(catcher));
		when(this.pluginSettings.get(Key.ENABLED.getKey())).thenReturn(Boolean.TRUE.toString());
		when(this.pluginSettings.get(Key.SERVERURL.getKey())).thenReturn(SERVERURL);
		when(this.pluginSettings.get(Key.CONSUMERNAME.getKey())).thenReturn(CONSUMERNAME);
		when(this.pluginSettings.get(Key.CONSUMERSECRET.getKey())).thenReturn(CONSUMERSECRET);

		assertTrue(this.config.isEnabled());
		// Cached, should not trigger pluginSettings read
		assertTrue(this.config.isEnabled());

		verify(this.pluginSettings).get(Key.ENABLED.getKey());
		verify(this.userStore).setServerUrl(eq(SERVERURL));
		assertEquals(CONSUMERNAME, catcher.getTokenPair().getToken());
		assertEquals(CONSUMERSECRET, catcher.getTokenPair().getSecret());
	}

	@Test
	public void testIsEnabledFalse() {
		when(this.pluginSettings.get(Key.ENABLED.getKey())).thenReturn(Boolean.FALSE.toString());
		assertFalse(this.config.isEnabled());
	}

	@Test
	public void testIsEnabledNull() {
		when(this.pluginSettings.get(Key.ENABLED.getKey())).thenReturn(null);
		assertFalse(this.config.isEnabled());
	}

	@Test
	public void testIsEnabledEmpty() {
		when(this.pluginSettings.get(Key.ENABLED.getKey())).thenReturn("");
		assertFalse(this.config.isEnabled());
	}

	@Test
	public void testSetEnabledTrue() {
		assertFalse(this.config.isEnabled());
		this.config.setEnabled(true);
		assertTrue(this.config.isEnabled());

		verify(this.pluginSettings).put(eq(Key.ENABLED.getKey()), eq(Boolean.TRUE.toString()));
	}

	@Test
	public void testSetEnabledFalse() {
		this.config.setEnabled(false);

		verify(this.pluginSettings).put(eq(Key.ENABLED.getKey()), eq(Boolean.FALSE.toString()));
	}

	@Test
	public void testGetServerUrl() {
		when(this.pluginSettings.get(Key.SERVERURL.getKey())).thenReturn(SERVERURL);
		assertEquals(SERVERURL, this.config.getServerUrl());
	}

	@Test
	public void testSetServerUrl() {
		this.config.setServerUrl(SERVERURL);

		verify(this.pluginSettings).put(eq(Key.SERVERURL.getKey()), eq(SERVERURL));
		verify(this.userStore).setServerUrl(eq(SERVERURL));
	}

	@Test
	public void testGetPublicUrl() {
		when(this.pluginSettings.get(Key.PUBLICURL.getKey())).thenReturn(PUBLICURL);
		assertEquals(PUBLICURL, this.config.getPublicUrl());
		// Cached, should not trigger pluginSettings read
		assertEquals(PUBLICURL, this.config.getPublicUrl());

		verify(this.pluginSettings).get(Key.PUBLICURL.getKey());
	}

	@Test
	public void testSetPublicUrl() {
		assertNull(this.config.getPublicUrl());
		this.config.setPublicUrl(PUBLICURL);
		assertEquals(PUBLICURL, this.config.getPublicUrl());

		verify(this.pluginSettings).put(eq(Key.PUBLICURL.getKey()), eq(PUBLICURL));
	}

	@Test
	public void testGetConsumerToken() {
		when(this.pluginSettings.get(Key.CONSUMERNAME.getKey())).thenReturn(CONSUMERNAME);
		when(this.pluginSettings.get(Key.CONSUMERSECRET.getKey())).thenReturn(CONSUMERSECRET);
		assertEquals(CONSUMERNAME, this.config.getConsumerToken().getToken());
		assertEquals(CONSUMERSECRET, this.config.getConsumerToken().getSecret());
	}

	@Test
	public void testSetConsumerToken() {
		this.config.setConsumerToken(CONSUMERTOKENPAIR);

		verify(this.pluginSettings).put(eq(Key.CONSUMERNAME.getKey()), eq(CONSUMERNAME));
		verify(this.userStore).setConsumerToken(eq(CONSUMERTOKENPAIR));
	}

	public static class OAuthTokenPairCatcher extends ArgumentMatcher<OAuthTokenPair> {
		private OAuthTokenPair tokenPair;

		public final OAuthTokenPair getTokenPair() {
			return this.tokenPair;
		}

		@Override
		public boolean matches(Object argument) {
			if (argument instanceof OAuthTokenPair) {
				this.tokenPair = (OAuthTokenPair) argument;
				return true;
			}
			return false;
		}
	}
}
