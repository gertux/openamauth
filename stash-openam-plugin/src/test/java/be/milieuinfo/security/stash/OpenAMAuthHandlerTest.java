package be.milieuinfo.security.stash;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import be.milieuinfo.security.openam.api.OpenAMIdentityService;
import be.milieuinfo.security.openam.api.OpenAMTokenManager;
import be.milieuinfo.security.openam.common.OpenAMTools;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettings;
import com.atlassian.stash.auth.HttpAuthenticationContext;
import com.atlassian.stash.auth.HttpAuthenticationFailureContext;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.i18n.KeyedMessage;
import com.atlassian.stash.user.ExpiredAuthenticationException;
import com.atlassian.stash.user.IncorrectPasswordAuthenticationException;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.user.UserService;

public class OpenAMAuthHandlerTest {
	private static final String PUBLIC_SSO_URL = "http://www.mmis.be/sso";
	private static final String REQUEST_URL = "http://git.mmis.be/stash";
	private static final String USERNAME = "jrr";
	private static final String SSO_TOKENID = "AQIC5wM2LY4RfckcedfzxGrgVYevbKR-SgBkuemF4Cmn5Qg.*AAJTSQABMDE.*";
	private static final Cookie SSO_COOKIE = new Cookie(OpenAMTokenManager.DEFAULT_COOKIENAME, SSO_TOKENID);
	private static final Cookie OTHER_COOKIE = new Cookie("JSESSIONID", "jfdsjfsdjfksdjfie");
	private static final Cookie[] COOKIES = new Cookie[] { OTHER_COOKIE, SSO_COOKIE };
	private static final KeyedMessage MSG = new KeyedMessage(OpenAMAuthHandler.OPENAM_AUTH_EXPIRED_MSG_KEY, "msg",
			"rootMessage");
	@Mock
	private UserService userService;
	@Mock
	private OpenAMIdentityService identityService;
	@Mock
	private CacheManager cacheManager;
	@Mock
	private Cache<String, String> authCache;
	@Mock
	private StoredConfig storedConfig;
	@Mock
	private I18nService i18nService;
	@Mock
	private HttpAuthenticationContext httpAuthenticationContext;
	@Mock
	private HttpAuthenticationFailureContext httpAuthenticationFailureContext;
	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;
	@Mock
	private StashUser stashUser;

	private OpenAMAuthHandler authHandler;

	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(this.httpAuthenticationContext.getRequest()).thenReturn(this.request);
		when(
				this.cacheManager.getCache(eq(OpenAMAuthHandler.OPENAM_AUTH_CACHE), any(CacheLoader.class),
						any(CacheSettings.class))).thenReturn(this.authCache);
		when(this.authCache.get(eq(SSO_TOKENID))).thenReturn(USERNAME);
		this.authHandler = new OpenAMAuthHandler(this.userService, this.identityService, this.cacheManager,
				this.storedConfig, this.i18nService);
		when(this.storedConfig.isEnabled()).thenReturn(Boolean.TRUE);
		when(this.httpAuthenticationContext.getMethod()).thenReturn(HttpAuthenticationContext.METHOD_TOKEN);
		when(this.userService.getUserByName(eq(USERNAME))).thenReturn(this.stashUser);
		when(
				this.i18nService.createKeyedMessage(eq(OpenAMAuthHandler.OPENAM_AUTH_EXPIRED_MSG_KEY),
						Matchers.anyVararg())).thenReturn(MSG);
		when(
				this.i18nService.createKeyedMessage(eq(OpenAMAuthHandler.OPENAM_AUTH_MISSING_MSG_KEY),
						Matchers.anyVararg())).thenReturn(MSG);
		when(this.httpAuthenticationFailureContext.getMethod()).thenReturn(HttpAuthenticationContext.METHOD_TOKEN);
		when(this.httpAuthenticationFailureContext.getRequest()).thenReturn(this.request);
		when(this.httpAuthenticationFailureContext.getResponse()).thenReturn(this.response);
		when(this.httpAuthenticationFailureContext.getAuthenticationException()).thenReturn(
				new OpenAMAuthException(MSG));
		when(this.storedConfig.getPublicUrl()).thenReturn(PUBLIC_SSO_URL);
		when(this.request.getRequestURL()).thenReturn(new StringBuffer(REQUEST_URL));
		when(this.request.getPathInfo()).thenReturn("");
	}

	@Test
	public void testAuthenticateToken() {
		when(this.request.getHeader(eq(OpenAMTools.OPENAM_SSO_ID_HEADER))).thenReturn(SSO_TOKENID);

		StashUser user = this.authHandler.authenticate(this.httpAuthenticationContext);
		assertEquals(this.stashUser, user);
	}

	@Test
	public void testAuthenticateTokenUserNotFound() {
		when(this.request.getHeader(eq(OpenAMTools.OPENAM_SSO_ID_HEADER))).thenReturn(SSO_TOKENID);
		when(this.userService.getUserByName(eq(USERNAME))).thenReturn(null);

		assertNull(this.authHandler.authenticate(this.httpAuthenticationContext));
	}

	@Test
	public void testAuthenticateTokenLoginPage() {
		when(this.request.getPathInfo()).thenReturn(OpenAMAuthHandler.LOGIN_PATHINFO);
		when(this.request.getHeader(eq(OpenAMTools.OPENAM_SSO_ID_HEADER))).thenReturn(SSO_TOKENID);

		assertNull(this.authHandler.authenticate(this.httpAuthenticationContext));
	}

	@Test
	public void testAuthenticateCookie() {
		when(this.request.getCookies()).thenReturn(COOKIES);

		StashUser user = this.authHandler.authenticate(this.httpAuthenticationContext);
		assertEquals(this.stashUser, user);
	}

	@Test
	public void testAuthenticateCookieLoginPage() {
		when(this.request.getPathInfo()).thenReturn(OpenAMAuthHandler.LOGIN_PATHINFO);
		when(this.request.getCookies()).thenReturn(COOKIES);

		assertNull(this.authHandler.authenticate(this.httpAuthenticationContext));
	}

	@Test(expected = OpenAMAuthException.class)
	public void testAuthenticateCookieNoDetails() {
		when(this.request.getCookies()).thenReturn(COOKIES);
		when(this.authCache.get(eq(SSO_TOKENID))).thenReturn(null);

		this.authHandler.authenticate(this.httpAuthenticationContext);
	}

	@Test(expected = OpenAMAuthException.class)
	public void testAuthenticateNoToken() {
		this.authHandler.authenticate(this.httpAuthenticationContext);
	}

	@Test
	public void testAuthenticateDisabled() {
		when(this.storedConfig.isEnabled()).thenReturn(Boolean.FALSE);
		when(this.request.getCookies()).thenReturn(COOKIES);

		assertNull(this.authHandler.authenticate(this.httpAuthenticationContext));
	}

	@Test
	public void testAuthenticateBasic() {
		when(this.request.getCookies()).thenReturn(COOKIES);
		when(this.httpAuthenticationContext.getMethod()).thenReturn(HttpAuthenticationContext.METHOD_BASIC);

		assertNull(this.authHandler.authenticate(this.httpAuthenticationContext));
	}

	@Test
	public void testAuthenticateForm() {
		when(this.request.getCookies()).thenReturn(COOKIES);
		when(this.httpAuthenticationContext.getMethod()).thenReturn(HttpAuthenticationContext.METHOD_FORM);

		assertNull(this.authHandler.authenticate(this.httpAuthenticationContext));
	}

	@Test
	public void testValidateAuthenticationToken() {
		when(this.request.getHeader(eq(OpenAMTools.OPENAM_SSO_ID_HEADER))).thenReturn(SSO_TOKENID);

		this.authHandler.validateAuthentication(this.httpAuthenticationContext);
	}

	@Test
	public void testValidateAuthenticationCookie() {
		when(this.request.getCookies()).thenReturn(COOKIES);

		this.authHandler.validateAuthentication(this.httpAuthenticationContext);
	}

	@Test
	public void testValidateAuthenticationNoToken() {
		this.authHandler.validateAuthentication(this.httpAuthenticationContext);
	}

	@Test(expected = ExpiredAuthenticationException.class)
	public void testValidateAuthenticationCookieInvalidToken() {
		when(this.request.getCookies()).thenReturn(COOKIES);
		when(this.authCache.get(eq(SSO_TOKENID))).thenReturn(null);

		this.authHandler.validateAuthentication(this.httpAuthenticationContext);
	}

	@Test
	public void testValidateAuthenticationDisabled() {
		when(this.request.getHeader(eq(OpenAMTools.OPENAM_SSO_ID_HEADER))).thenReturn(SSO_TOKENID);
		when(this.storedConfig.isEnabled()).thenReturn(Boolean.FALSE);

		this.authHandler.validateAuthentication(this.httpAuthenticationContext);
	}

	@Test
	public void testValidateAuthenticationBasic() {
		when(this.request.getHeader(eq(OpenAMTools.OPENAM_SSO_ID_HEADER))).thenReturn(SSO_TOKENID);
		when(this.httpAuthenticationContext.getMethod()).thenReturn(HttpAuthenticationContext.METHOD_BASIC);

		this.authHandler.validateAuthentication(this.httpAuthenticationContext);
	}

	@Test
	public void testValidateAuthenticationForm() {
		when(this.request.getHeader(eq(OpenAMTools.OPENAM_SSO_ID_HEADER))).thenReturn(SSO_TOKENID);
		when(this.httpAuthenticationContext.getMethod()).thenReturn(HttpAuthenticationContext.METHOD_FORM);

		this.authHandler.validateAuthentication(this.httpAuthenticationContext);
	}

	@Test
	public void testOnAuthenticationFailure() throws Exception {
		assertTrue(this.authHandler.onAuthenticationFailure(this.httpAuthenticationFailureContext));

		verify(this.response).sendRedirect(anyString());
	}

	@Test
	public void testOnAuthenticationFailureOtherAuthenticationException() throws Exception {
		when(this.httpAuthenticationFailureContext.getAuthenticationException()).thenReturn(
				new IncorrectPasswordAuthenticationException(MSG));

		assertFalse(this.authHandler.onAuthenticationFailure(this.httpAuthenticationFailureContext));
	}

	@Test
	public void testOnAuthenticationFailureDisabled() throws Exception {
		when(this.storedConfig.isEnabled()).thenReturn(Boolean.FALSE);

		assertFalse(this.authHandler.onAuthenticationFailure(this.httpAuthenticationFailureContext));
	}

	@Test
	public void testOnAuthenticationFailureBasic() throws Exception {
		when(this.httpAuthenticationFailureContext.getMethod()).thenReturn(HttpAuthenticationContext.METHOD_BASIC);

		assertFalse(this.authHandler.onAuthenticationFailure(this.httpAuthenticationFailureContext));
	}

	@Test
	public void testOnAuthenticationFailureForm() throws Exception {
		when(this.httpAuthenticationFailureContext.getMethod()).thenReturn(HttpAuthenticationContext.METHOD_FORM);

		assertFalse(this.authHandler.onAuthenticationFailure(this.httpAuthenticationFailureContext));
	}

	@Test
	public void testGetLoginUrl() {
		assertEquals(PUBLIC_SSO_URL + "?goto=http%3A%2F%2Fgit.mmis.be%2Fstash",
				this.authHandler.getLoginUrl(this.request));
	}
}
